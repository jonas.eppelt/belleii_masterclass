from setuptools import setup, find_packages

setup(
    name="belleIImasterclass",
    version="0.2.0",
    packages=find_packages(),
    license="",
    author="Filip Gostner, Isabel Haide, Jonas Eppelt",
    description="A BelleII Masterclass",
    
)
